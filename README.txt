date_tokens

README

Description
------------

This module defines a framework for adding tokens to date filters.  By default,
it defines the "@ENDOFMONTH" token.  You could create a filter on your view
that would show all events such that their date was <= @ENDOFMONTH.  This will
evaluate to the end of the current month.

This module also defines the hook hook_date_tokens_tokens so that you can add
your own tokens.

hook_date_tokens_tokens() returns an array:
  array(
    '@TOKEN_NAME' => 'callback_name',
    ...
  );

The callback is called like so:
  callback($token_value, $filter, $view, {'jscal'/'sql'}, $relative_to, $form);

The callback should return a formatted date.

For an example of this callback, look at _date_tokens_end_of_month.

Installation
------------

Unpack the tarball and install in your modules directory.  Enable it on the
modules admin page.  In order to install modules, though, you'll need to make
the directory you intend to install to writable by the web-server user.
